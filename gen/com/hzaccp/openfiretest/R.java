/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.hzaccp.openfiretest;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int filedialog_file=0x7f020000;
        public static final int filedialog_folder=0x7f020001;
        public static final int filedialog_folder_up=0x7f020002;
        public static final int filedialog_root=0x7f020003;
        public static final int filedialog_wavfile=0x7f020004;
        public static final int ic_launcher=0x7f020005;
    }
    public static final class id {
        public static final int ItemTitle=0x7f080011;
        public static final int LinearLayout1=0x7f080000;
        public static final int action_settings=0x7f080013;
        public static final int btnCapture=0x7f080008;
        public static final int btnFile=0x7f080009;
        public static final int btnLogin=0x7f080001;
        public static final int btnSend=0x7f08000a;
        public static final int filedialogitem_img=0x7f08000c;
        public static final int filedialogitem_name=0x7f08000d;
        public static final int filedialogitem_path=0x7f08000e;
        public static final int linMess=0x7f080006;
        public static final int txtChat=0x7f080007;
        public static final int txtIP=0x7f080004;
        public static final int txtPassword=0x7f080003;
        public static final int txtPort=0x7f080005;
        public static final int txtUserName=0x7f080002;
        public static final int userId=0x7f080010;
        public static final int userListView=0x7f080012;
        public static final int userName=0x7f08000f;
        public static final int vw1=0x7f08000b;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int chat=0x7f030001;
        public static final int filedialogitem=0x7f030002;
        public static final int listrow=0x7f030003;
        public static final int users=0x7f030004;
    }
    public static final class menu {
        public static final int main=0x7f070000;
    }
    public static final class string {
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int hello_world=0x7f050002;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
